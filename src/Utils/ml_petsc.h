/*!
 * \file ml_petsc.h
 *
 * \brief ML wrappers for PETSc data stuctures.
 *
 */
/* ******************************************************************** */
/* See the file COPYRIGHT for a complete copyright notice, contact      */
/* person and disclaimer.                                               */        
/* ******************************************************************** */
/*#############################################################################
# CVS File Information
#    Current revision: $Revision: 1.1.2.1 $
#    Last modified:    $Date: 2008/08/30 00:07:03 $
#    Modified by:      $Author: jhu $
#############################################################################*/

#ifndef ML_PETSC_H
#define ML_PETSC_H

#ifdef HAVE_PETSC

#ifdef __cplusplus
extern "C" {
#endif
#include "petscksp.h"
#ifdef __cplusplus
}
#endif

/*wrap PETSc's pointers to preconditioner and solver structures*/
typedef PC ML_PetscPC;
typedef KSP ML_PetscKSP;

#endif /*ifdef HAVE_PETSC*/

#endif /* define ML_PETSC_H */
